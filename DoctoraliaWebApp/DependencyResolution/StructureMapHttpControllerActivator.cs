﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;

namespace DoctoraliaWebApp.DependencyResolution
{
    public class StructureMapHttpControllerActivator : IHttpControllerActivator
    {
        private readonly IContainer container;

        public StructureMapHttpControllerActivator(IContainer container)
        {
            this.container = container;
        }

        public IHttpController Create(
                HttpRequestMessage request,
                HttpControllerDescriptor controllerDescriptor,
                Type controllerType)
        {
            return (IHttpController)this.container.GetInstance(controllerType);
        }
    }
}