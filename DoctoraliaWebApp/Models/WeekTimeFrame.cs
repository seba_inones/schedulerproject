﻿using DoctoraliaWebApp.Models;
using DoctoraliaWebApp.RestApi.Models;
using System.Collections.Generic;

namespace DoctoraliaWebApp.Models
{
    public class WeekTimeFrame
    {
        public List<Slots> Slots { get; set; }
    }
}