﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoctoraliaWebApp.Models
{
    public class WeekAvailability
    {
        public List<DateTime> Dates { get; set; }
        public List<WeekTimeFrame> WeekTimeFrames { get; set; }

        public WeekAvailability()
        {
            WeekTimeFrames = new List<WeekTimeFrame>();

        }
    }
}