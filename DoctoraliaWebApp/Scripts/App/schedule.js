﻿var scheduleApp = angular.module('scheduleApp', ["ngResource"]);

scheduleApp.factory('ScheduleService', function ($resource) {

    var factory = {};

    factory.GetCurrentWeek = function() {

         return $resource(
            "/api/ScheduleRest/Get/:Id",                        
            { Id: "@Id" },
            { "get": { method: "GET" } }
             );
    }

    factory.GetNextWeek = function () {

        return $resource(
               "/api/ScheduleRest/NextWeek/:Id",
               { Id: "@Id" },
               { "get": { method: "GET" } }
          );
    }

    factory.GetlastWeek = function () {

        return $resource(
            "/api/ScheduleRest/PreviousWeek/:Id",
            { Id: "@Id" },
            { "get": { method: "GET" } }
       );
    }
    return factory;
});


//Remember to inject its dependecies
scheduleApp.controller('scheduleController', function ($scope, ScheduleService) {
    console.log($scope);

    $scope.HeadingCaption = "Free Slots";

    $scope.callScheduleService = function () {
        //$scope.scheduleAvailability = ScheduleService.query(); --> For a List
        $scope.scheduleAvailability = ScheduleService.GetCurrentWeek().get();
    };

    $scope.callNextWeek = function () {        
        $scope.scheduleAvailability = ScheduleService.GetNextWeek().get();
        console.log($scope.scheduleAvailability);
    };
    $scope.callLastWeek = function () {        
        $scope.scheduleAvailability = ScheduleService.GetlastWeek().get();
        console.log($scope.scheduleAvailability);
    };

    $scope.callScheduleService();
});
