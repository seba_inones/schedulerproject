﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoctoraliaWebApp.Common
{
    public class DateProvider : IDateProvider
    {

        private const int DaysPerWeek = 7;

        public DateTime GetPreviousWeekDate(DateTime currentDate)
        {
            return currentDate.AddDays(-7.0);
        }

        public DateTime GetNextWeekDate(DateTime currentDate)
        {
            return currentDate.AddDays(7.0);
        }
        
        public List<DateTime> GetWeekConsecutiveDates(DateTime initialDayOfWeek)
        {
            List<DateTime> weekConsecutiveDates = new List<DateTime>();
            var currentDay = initialDayOfWeek;

            for (int weekday = 0; weekday < DaysPerWeek; weekday++)
            {
                weekConsecutiveDates.Add(currentDay);
                currentDay = currentDay.AddDays(1);
            }

            return weekConsecutiveDates;
        }
    }
}