﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoctoraliaWebApp.Common
{
    public static class Converter
    {
        public static T ConvertValue<T>(object value)
        {
            return (T)Convert.ChangeType(value, typeof(T));
        }
    }
}