﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoctoraliaWebApp.Common
{
    public interface IDateProvider
    {
        DateTime GetNextWeekDate(DateTime currentDate);
        DateTime GetPreviousWeekDate(DateTime currentDate);
        List<DateTime> GetWeekConsecutiveDates(DateTime initialDayOfWeek);
    }
}
