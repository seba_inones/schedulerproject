﻿using DoctoraliaWebApp.Common;
using DoctoraliaWebApp.Models;
using DoctoraliaWebApp.RestApi;
using DoctoraliaWebApp.RestApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace DoctoraliaWebApp.Controllers
{
    public class ScheduleRestController : ApiController
    {
        private readonly IRestClient restClient;
        private readonly IDateProvider dateProvider;

        //TODO: Some way to make it not static and being valid.
        private static DateTime currentDate;

        public ScheduleRestController(IRestClient restClient, IDateProvider dateProvider)
        {
            this.restClient = restClient;
            this.dateProvider = dateProvider;
        }

        // GET: api/ScheduleRest
        [HttpGet]
        [ActionName("Get")]
        public async Task<WeekAvailability> Get()
        {
            currentDate = DateTime.Now;

            return await GetWeekAvailability();
        }

        [HttpGet]
        [ActionName("NextWeek")]
        public async Task<WeekAvailability> GetNextWeek()
        {
            currentDate = dateProvider.GetNextWeekDate(currentDate);

            return await GetWeekAvailability();
        }

        [HttpGet]
        [ActionName("PreviousWeek")]
        public async Task<WeekAvailability> GetPreviousWeek()
        {
            currentDate = dateProvider.GetPreviousWeekDate(currentDate);

            return await GetWeekAvailability();

        }

        // POST: api/ScheduleRest
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/ScheduleRest/5
        public void Put(int id, [FromBody]string value)
        {
        }

        private async Task<WeekAvailability> GetWeekAvailability()
        {
            WeekAvailability weekAvailability = new WeekAvailability();

            var schedule = await restClient.Get<Schedule>(currentDate);

            SetWeekAvailability(weekAvailability, schedule);

            return weekAvailability;
        }

        private void SetWeekAvailability(WeekAvailability weekAvailability, Schedule schedule)
        {
            weekAvailability.Dates = GetDates(schedule.Availability);
            weekAvailability.WeekTimeFrames = Pivot(schedule.Availability, weekAvailability.Dates);
        }

        private List<WeekTimeFrame> Pivot(List<Availability> availabilityList, List<DateTime> consecutiveWeekDays)
        {
            List<WeekTimeFrame> weekTimeFrame = new List<WeekTimeFrame>();

            int maximumSlotNumber = GetMaximumSlot(availabilityList);

            for (int slotNumber = 0; slotNumber < maximumSlotNumber; slotNumber++)
            {
                var currentWeekTimeFrame = new WeekTimeFrame();

                currentWeekTimeFrame.Slots = GetTimeFrame(availabilityList, slotNumber, consecutiveWeekDays);

                weekTimeFrame.Add(currentWeekTimeFrame);
            }

            return weekTimeFrame;
        }

        private List<DateTime> GetDates(List<Availability> availabilityList)
        {
            List<DateTime> weekConsecutivedates = dateProvider.GetWeekConsecutiveDates(availabilityList.FirstOrDefault().Date);

            return weekConsecutivedates;
        }

        private int GetMaximumSlot(List<Availability> availabilityList)
        {
            int maximumSlotNumber = 0;
            foreach (var availability in availabilityList)
            {
                if (availability.Slots.Count > maximumSlotNumber)
                    maximumSlotNumber = availability.Slots.Count;
            }

            return maximumSlotNumber;
        }

        //TODO: Improve this.
        //You must show days without available slots too
        private List<Slots> GetTimeFrame(List<Availability> availabilityList, int slotNumber, List<DateTime> consecutiveWeekDays)
        {
            List<Slots> timeFrame = new List<Slots>();
            int currentDayCounter = 0;

            foreach (var consecutiveDay in consecutiveWeekDays)
            {
                if (currentDayCounter < availabilityList.Count)
                {
                    var currentDay = availabilityList[currentDayCounter].Date;

                    if (consecutiveDay == currentDay)
                    {
                        if (HasAvailableSlots(availabilityList, slotNumber, currentDayCounter))
                            timeFrame.Add(availabilityList[currentDayCounter].Slots[slotNumber]);
                        else
                            timeFrame.Add(new Slots());
                    }
                    else
                        CreateDayWithoutAvailableSlots(availabilityList, slotNumber, timeFrame, currentDayCounter, consecutiveDay);
                }
                else
                    CreateDayWithoutAvailableSlots(availabilityList, slotNumber, timeFrame, currentDayCounter, consecutiveDay);

                currentDayCounter++;
            }

            return timeFrame;
        }

        private static bool HasAvailableSlots(List<Availability> availabilityList, int slotNumber, int currentDayCounter)
        {
            return slotNumber < availabilityList[currentDayCounter].Slots.Count;
        }

        private static void CreateDayWithoutAvailableSlots(List<Availability> availabilityList, int slotNumber, List<Slots> timeFrame, int currentDayCounter, DateTime consecutiveDay)
        {
            availabilityList.Insert(currentDayCounter, new Availability()
            {
                Date = consecutiveDay,
                Slots = new List<Slots>() { new Slots() }
            });

            timeFrame.Add(availabilityList[currentDayCounter].Slots[slotNumber]);
        }
    }
}
