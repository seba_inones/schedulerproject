﻿using System;
using System.Threading.Tasks;

namespace DoctoraliaWebApp.RestApi
{
    public interface IRestClient
    {
        string ApiKey { get; }
        string ApiName { get; }
        string ScheduleId { get; }

        Task<T> Get<T>(DateTime dateTime);
    }
}