﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DoctoraliaWebApp.RestApi.Models
{
    public class Slots
    {
        public string Start { get; set; }
        public string Duration { get; set; }
        public string _Id { get; set; }
    }
}
