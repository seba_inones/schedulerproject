﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DoctoraliaWebApp.RestApi.Models
{
    public class Availability
    {
        public DateTime Date { get; set; }
        public List<Slots> Slots { get; set; }
        public string Synced { get; set; }
    }
}
