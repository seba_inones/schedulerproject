﻿using System.Collections.Generic;

namespace DoctoraliaWebApp.RestApi.Models
{
    public class Schedule
    {
        public string Id { get; set; }
        public List<Availability> Availability { get; set; }
    }
}