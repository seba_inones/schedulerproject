﻿using DoctoraliaWebApp.RestApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoctoraliaWebApp.RestApi.Models
{
    public class ResultMessage
    {
        public string Success { get; set; }
        public Schedule Schedule { get; set; }
    }
}