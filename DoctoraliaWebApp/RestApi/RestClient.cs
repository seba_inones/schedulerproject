﻿using DoctoraliaWebApp.Common;
using DoctoraliaWebApp.RestApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Text;
using System.Threading.Tasks;

namespace DoctoraliaWebApp.RestApi
{
    public class RestClient : IRestClient
    {
        private readonly JsonSerializerSettings jsonSerializerSettings;

        public string ApiName
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["ApiName"]; }
        }

        public string ApiKey
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["ApiKey"]; }
        }

        public string ScheduleId
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["ScheduleId"]; }
        }

        public RestClient()
        {
            jsonSerializerSettings = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
        }

        public async Task<T> Get<T>(DateTime dateTime)
        {
            T jsonObject = default(T);
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    SetHeaders(client);

                    //TODO: Verify this!! This workaround could be potential security threat as you are turning off the SSL certificate validation.
                    ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });

                    //TODO: Improve this -->Encapsulate Logic

                    var currentDate = dateTime.ToString("yyyyMMdd");

                    Uri apiUri = new Uri("https://medeox-staging.cloudapp.net/v1/consumer/schedules/");
                    UriBuilder uriBuilder = new UriBuilder(apiUri);
                    uriBuilder.Path += Path.Combine(ScheduleId, currentDate);

                    using (HttpResponseMessage response = client.GetAsync(uriBuilder.Uri).Result)
                    {
                        response.EnsureSuccessStatusCode();
                        string responseBody = await response.Content.ReadAsStringAsync();
                        ResultMessage responseJson = JsonConvert.DeserializeObject<ResultMessage>(responseBody, jsonSerializerSettings);

                        jsonObject = Converter.ConvertValue<T>(responseJson.Schedule);
                    }
                }
            }

            catch (JsonException exception)
            {
                throw new Exception(exception.Message);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }

            return jsonObject;
        }

        private void SetHeaders(HttpClient client)
        {
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", ApiName, ApiKey))));
        }


        public async Task<T> Post<T>(string url, object requestContent)
        {

            //var jsonRequestContent = JsonConvert.SerializeObject(requestContent, JsonUtilities.GetJsonSerializerSettings());

            T jsonObject = default(T);
            HttpResponseMessage response = null;
            try
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //response = await client.PostAsync(new Uri(url), new StringContent(jsonRequestContent, Encoding.UTF8, "application/json"));
                if (!response.IsSuccessStatusCode)
                {
                    if (response.StatusCode == HttpStatusCode.NotFound)
                        Console.WriteLine("Resource not found in server");
                    response.EnsureSuccessStatusCode();
                }
                var responseContentString = await response.Content.ReadAsStringAsync();
                //var responseJson = JsonConvert.DeserializeObject<JsonServiceResult>(responseContentString);
                //jsonObject = JsonConvert.DeserializeObject<T>(responseJson.content.ToString(), JsonUtilities.GetJsonSerializerSettings());
            }
            catch (JsonException exception)
            {
                Console.WriteLine(exception.Message);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }

            return jsonObject;
        }

    }

}